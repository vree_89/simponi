@extends('menu.layout.header-menu')

@section('main')
  <div class="main">
    <div class="main-content">
			<div class="container-fluid">
				<div class="row">
          <div class="col-md-12">
  					<div class="panel">
  						<div class="panel-heading">
  							<h3 class="panel-title">Rekapitulasi Opini</h3>
  						</div>
  						<div class="panel-body">
                <form class="" action="/input_pengajuan" method="post">
                  <table style="width:100%">
                    <tr>
                      <td width="50%">
                        Start from
                        <input type="text" name="maker" id="maker" class="form-control" style="width:95%">
                      </td>
                      <td width="50%">
                        To<br>
                        <input type="text" name="namensbh" id="namensbh" class="form-control" required>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <br>
                        <button type="submit" class="btn btn-primary btn-toastr" data-context="info" data-message="This is general theme info" data-position="top-right">Cari</button>
                      </td>
                      <td>

                      </td>
                    </tr>
                  </table>
                </form>
                <br>
                <div class="table-responsive">
    							<table class="table table-bordered table-hover" id="list">
    								<thead>
    									<tr>
                        @foreach ($columns as $columns)
                          <th style="text-align:center" nowrap>{{ strtoupper(str_replace("_"," ",$columns)) }}</th>
                        @endforeach
    									</tr>
    								</thead>
    								<tbody>
                      @foreach ($editpeng as $edtpeng)
                        <tr>
                          <td nowrap>{{ $edtpeng->Id }}</td>
                          <td nowrap>{{ $edtpeng->maker }}</td>
                          <td nowrap>{{ $edtpeng->nama_nasabah }}</td>
                          <td nowrap>{{ $edtpeng->badan_usaha }}</td>
                          <td nowrap>{{ $edtpeng->cabang }}</td>
                          <td nowrap>{{ $edtpeng->nama_ao_pic }}</td>
                          <td nowrap>{{ $edtpeng->nama_supervisi }}</td>
                          <td nowrap>{{ $edtpeng->plafond }}</td>
                          <td nowrap>{{ $edtpeng->tujuan_opini }}</td>
                          <td nowrap>{{ $edtpeng->no_memo_pengajuan }}</td>
                          <td nowrap>{{ $edtpeng->tgl_masuk_pengajuan }}</td>
                          <td nowrap>{{ $edtpeng->jam_memo_diterima }}</td>
                          <td nowrap>{{ $edtpeng->tgl_disposisi }}</td>
                          <td nowrap>{{ $edtpeng->jam_disposisi }}</td>
                          <td nowrap>{{ $edtpeng->tgl_lengkap }}</td>
                          <td nowrap>{{ $edtpeng->jam_lengkap }}</td>
                        </tr>
                      @endforeach
    								</tbody>
    							</table>
    						</div>
                {{ $editpeng->links() }}
              </div>
  					</div>
          </div>
				</div>
			</div>
		</div>
  </div>
@endsection


@section('style')
  <script>
    @if (Session::has('message'))
      var type = "{{Session::get('alert-type','info')}}"

      switch (type) {
          case 'sukseshapus':
            toastr.success("{{ Session::get('message') }}");
            break;
          case 'gagalhapus':
            toastr.error("{{ Session::get('message') }}");
            break;
          case 'suksessimpan':
            toastr.success("{{ Session::get('message') }}");
            break;
          case 'gagalsimpan':
            toastr.error("{{ Session::get('message') }}");
            break;
        }
		@endif
	</script>
@endsection
