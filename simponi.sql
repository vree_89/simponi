﻿# Host: localhost  (Version 5.5.5-10.1.26-MariaDB)
# Date: 2018-06-06 04:35:37
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "badan_usaha"
#

DROP TABLE IF EXISTS `badan_usaha`;
CREATE TABLE `badan_usaha` (
  `Id` varchar(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "badan_usaha"
#

INSERT INTO `badan_usaha` VALUES ('1','Perseorangan'),('2','PT'),('3','CV'),('4','Yayasan'),('5','Muhammadiyah'),('6','Lain-Lain');

#
# Structure for table "cabang"
#

DROP TABLE IF EXISTS `cabang`;
CREATE TABLE `cabang` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lokasi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

#
# Data for table "cabang"
#

INSERT INTO `cabang` VALUES (1,'Bandung'),(2,'Bukittinggi '),(3,'Divisi Bisnis Area I / Salemba'),(4,'Divisi Bisnis Area II / Melawai'),(5,'Divisi Bisnis Area III / Kelapa Gading'),(6,'Divisi Bisnis Area IV / Bekasi'),(7,'Divisi Bisnis Area V / BSD'),(8,'Divisi Bisnis Area VI / Kramat Jati'),(9,'Lending Komersil'),(10,'Makasar'),(11,'Medan'),(12,'Samarinda'),(13,'Semarang'),(14,'Sidoarjo'),(15,'Solo'),(16,'Surabaya'),(17,'Yogyakarta'),(18,'Mikro'),(19,'RPP'),(20,'Divisi Teknologi Informasi (TI)'),(21,'Divisi Sumber Daya Insani (SDI)'),(22,'Divisi Pengembangan Bisnis & Perencanaan Strategis'),(23,'Divisi Supervisi Bisnis'),(24,'Divisi Pelayanan'),(25,'Divisi Support Pembiayaan'),(26,'Divisi Pembiayaan Komersial'),(27,'Divisi Pendanaan Institusi'),(28,'Divisi Bisnis Mikro'),(29,'Divisi Manajemen Penjualan'),(30,'Divisi Restrukturisasi & Penyelesaian Pembiayaan (RPP)'),(31,'Unit Anti Fraud');

#
# Structure for table "daftar_pengajuan"
#

DROP TABLE IF EXISTS `daftar_pengajuan`;
CREATE TABLE `daftar_pengajuan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `maker` varchar(255) NOT NULL DEFAULT '',
  `nama_nasabah` varchar(255) NOT NULL DEFAULT '',
  `badan_usaha` int(11) NOT NULL,
  `cabang` int(11) NOT NULL,
  `nama_ao_pic` varchar(255) NOT NULL DEFAULT '',
  `nama_supervisi` varchar(255) NOT NULL DEFAULT '',
  `plafond` decimal(16,0) NOT NULL DEFAULT '0',
  `tujuan_opini` int(11) NOT NULL,
  `no_memo_pengajuan` varchar(20) NOT NULL DEFAULT '',
  `tgl_masuk_pengajuan` date NOT NULL,
  `jam_memo_diterima` time NOT NULL,
  `tgl_disposisi` date NOT NULL,
  `jam_disposisi` time NOT NULL,
  `tgl_lengkap` date NOT NULL,
  `jam_lengkap` time NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

#
# Data for table "daftar_pengajuan"
#

INSERT INTO `daftar_pengajuan` VALUES (9,'Andriyan','Maryati',1,5,'Om Om','Dex',7676767,3,'4454545','2018-05-17','07:54:02','2018-05-10','22:54:02','2018-05-09','07:54:02','ON PROGRESS'),(10,'dimas','Mulyono',1,1,'Kendat','Eman',343,1,'345','2018-05-05','17:22:01','2018-05-05','17:22:01','2018-05-05','17:22:01','ON PROGRESS'),(11,'Dimas Bayu Andriyan','Mulyono',1,1,'Kendat','Eman',345,1,'345435','2018-05-06','07:40:02','2018-05-06','07:40:02','2018-05-06','07:40:02','ON PROGRESS'),(12,'Dimas Bayu Andriyan','Mulyono',1,1,'asdsa','asdasd',50000,1,'234324','2018-05-06','10:05:46','2018-05-06','10:05:46','2018-05-06','10:05:46','ON PROGRESS'),(13,'Dimas Bayu Andriyan','Malik',1,1,'Michael','Yono',5555,1,'5555','2018-05-25','19:10:26','2018-05-06','19:10:26','2018-05-06','19:10:26','ON PROGRESS'),(14,'Dimas Bayu Andriyan','Lucinta Luna',1,9,'Alex Belek','Mat Solar',5555,1,'5555','2018-05-06','22:10:30','2018-05-06','22:10:30','2018-05-06','22:10:30','ON PROGRESS'),(15,'Dimas Bayu Andriyan','Mandala',1,1,'Sumeneb','Jancok',4345,1,'35435345','2018-05-06','22:21:41','2018-05-06','22:21:41','2018-05-06','22:21:41','ON PROGRESS'),(16,'Dimas Bayu Andriyan','Iron Man',1,1,'Sumiat','Maslikun',435435,1,'435345','2018-05-06','22:22:01','2018-05-06','22:22:01','2018-05-06','22:22:01','ON PROGRESS'),(17,'Dimas Bayu Andriyan','Kapten Amerika',2,29,'Paijo','Wakijan',345435,6,'345345','2018-05-06','22:22:42','2018-05-06','22:22:42','2018-05-06','22:22:42','ON PROGRESS'),(18,'Dimas Bayu Andriyan','assad',1,33,'asd','asd',24332,7,'ewr','2018-05-06','22:23:38','2018-05-06','22:23:38','2018-05-06','22:23:38','ON PROGRESS'),(19,'Dimas Bayu Andriyan','sdf',1,1,'sdf','sdfsdf',32432,1,'4324234','2018-05-06','22:24:04','2018-05-06','22:24:04','2018-05-06','22:24:04','ON PROGRESS'),(20,'Dimas Bayu Andriyan','sdsf',1,1,'sdf','sdf',323432,1,'sd','2018-05-06','22:24:18','2018-05-06','22:24:18','2018-05-06','22:24:18','ON PROGRESS'),(21,'Dimas Bayu Andriyan','PT Sejahtera',1,27,'DENI','DIBAY',5000000000,1,'1234','2018-05-04','09:00:04','2018-05-04','09:10:04','2018-05-05','09:00:04','ON PROGRESS');

#
# Structure for table "tests"
#

DROP TABLE IF EXISTS `tests`;
CREATE TABLE `tests` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tests"
#


#
# Structure for table "tuj_pengajuan_opini"
#

DROP TABLE IF EXISTS `tuj_pengajuan_opini`;
CREATE TABLE `tuj_pengajuan_opini` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for table "tuj_pengajuan_opini"
#

INSERT INTO `tuj_pengajuan_opini` VALUES (1,'Baru'),(2,'Perpanjangan'),(3,'Restructure'),(4,'Penambahan'),(5,'Perubahan'),(6,'Reschedule'),(7,'AYDA');

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `level_user` varchar(10) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "user"
#

INSERT INTO `user` VALUES ('123','123','Rudi Iyut Sugianto','user'),('dimas','bayu','Dimas Bayu Andriyan','admin');
