<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use Illuminate\Pagination\Paginator;

use App\Models\pengajuan_opini;
use App\Models\badan_usaha;
use App\Models\cabang;
use App\Models\tujuan_pengajuan;
use App\Models\User;
use Session;

class edit_pengajuan_con extends Controller
{
    public function show(){

      $columns = DB::getSchemaBuilder()->getColumnListing('daftar_pengajuan');
      // $editpeng = pengajuan_opini::simplePaginate(10);
      // dd($editpeng);
      Session::forget('s_list_user');
      // return view('/menu/list_pengajuan', ['columns' => $columns, 'columns2' => $columns , 'editpeng' => $editpeng]);
      return view('/menu/list_pengajuan', ['columns2' => $columns]);
    }

    public function f_list_opini(Request $request){
        Session::put('s_list_user','x');
        $columns = DB::getSchemaBuilder()->getColumnListing('daftar_pengajuan');
        // dd($request->input('kriteria'));
        // Session::put('kriteria',$request->input('kriteria'));
        $q = $request->input('kriteria');
        if ($q == 'all') {
          // $editpeng = pengajuan_opini::where($request->input('kriteria'), 'like', '%'.$request->tekskriteria.'%')->orderBy('tgl_masuk_pengajuan', 'desc')->simplePaginate(10);
          // dd('all');
          $editpeng = pengajuan_opini::orderBy('tgl_masuk_pengajuan', 'desc')->paginate(10);
        }else if ($q == 'Id'){
          $editpeng = pengajuan_opini::where($request->input('kriteria'), '=', $request->tekskriteria)->orderBy('tgl_masuk_pengajuan', 'desc')->paginate(10);
        }else {
          $editpeng = pengajuan_opini::where($request->input('kriteria'), 'like', '%'.$request->tekskriteria.'%')->orderBy('tgl_masuk_pengajuan', 'desc')->paginate(10);
        }

        // $columns = DB::getSchemaBuilder()->getColumnListing('daftar_pengajuan');
        // $editpeng = pengajuan_opini::simplePaginate(10);
        return view('/menu/list_pengajuan', ['columns' => $columns, 'columns2' => $columns , 'editpeng' => $editpeng]);

        // dd($editpeng);

    }

    public function select($id){
      $data = pengajuan_opini::where('Id', '=', $id)->get();

      $badan_usaha = badan_usaha::all();
      $cabang = cabang::all();
      $tujuan_pengajuan = tujuan_pengajuan::all();
      // dd($data);
      // viewedit($id);
      // return redirect('/edit');
      return view('menu/edit_pengajuan',['data' => $data , 'badan_usaha' => $badan_usaha , 'cabang' => $cabang , 'tujuan_pengajuan' => $tujuan_pengajuan]);
    }

    public function update(Request $request){
      try {
        pengajuan_opini::where('id', $request->idopini)
                              ->update(['nama_nasabah' => $request->namensbh,
                                        'badan_usaha' => $request->input('bdnusaha'),
                                        'cabang' => $request->input('cbg'),
                                        'nama_ao_pic' => $request->namaaopic,
                                        'nama_supervisi' => $request->nmsupervisi,
                                        'plafond' => $request->plafond,
                                        'tujuan_opini' => $request->input('tujopini'),
                                        'no_memo_pengajuan' => $request->nomemo,
                                        'tgl_masuk_pengajuan' => $request->tglmasuk,
                                        'jam_memo_diterima' => $request->jammasuk,
                                        'tgl_disposisi' => $request->tgldisposisi,
                                        'jam_disposisi' => $request->jamdisposisi,
                                        'tgl_lengkap' => $request->tgllengkap,
                                        'jam_lengkap' => $request->jamlengkap
                                      ]);
        $notification = array(
                  'message' => 'Data berhasil diubah!',
                  'alert-type' => 'suksessimpan'
              );
      } catch (\Exception $e) {
        $notification = array(
                  'message' => 'Data gagal diubah!',
                  'alert-type' => 'gagalsimpan'
              );
      }

      return redirect('/list_pengajuan')->with($notification);
    }
    // public function viewedit(){
    //   $badan_usaha = badan_usaha::all();
    //   $cabang = cabang::all();
    //   $tujuan_pengajuan = tujuan_pengajuan::all();
    //   $data = pengajuan_opini::where('Id', '=', $id)->get();
    //
    //   dd($data);
    //
    //   return view('menu/edit_pengajuan',['data' => $data , 'badan_usaha' => $badan_usaha , 'cabang' => $cabang , 'tujuan_pengajuan' => $tujuan_pengajuan]);
    // }

    public function delete($id){
      try {
        pengajuan_opini::where('Id', '=', $id)->delete();

        $notification = array(
                  'message' => 'Data berhasil dihapus!',
                  'alert-type' => 'sukseshapus'
              );
        return back()->with($notification);
      } catch (\Exception $e) {
        $notification = array(
                  'message' => 'Data gagal dihapus!',
                  'alert-type' => 'gagalhapus'
              );
      }


    }
}
