<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\pengajuan_opini;
use App\Models\badan_usaha;
use App\Models\cabang;
use App\Models\tujuan_pengajuan;
use App\Models\User;
use Session;

class rekapitulasi_opini_con extends Controller
{
  public function show(){

    $columns = DB::getSchemaBuilder()->getColumnListing('daftar_pengajuan');
    $editpeng = pengajuan_opini::simplePaginate(10);
    // dd($editpeng);
    return view('/menu/rekapitulasi_opini', ['columns' => $columns, 'editpeng' => $editpeng]);
  }
}
