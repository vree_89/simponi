<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class badan_usaha extends Model
{
  protected $table = 'badan_usaha';

  // protected $guarded = ['id'];
  public $timestamps = false;
}
