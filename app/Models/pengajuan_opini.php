<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pengajuan_opini extends Model
{
    protected $table = 'daftar_pengajuan';

    // protected $guarded = ['id'];
    public $timestamps = false;
}
